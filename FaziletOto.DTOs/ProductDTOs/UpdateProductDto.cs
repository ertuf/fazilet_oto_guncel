﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.ProductDTOs
{
    public class UpdateProductDto : AddProductDto
    {
        [Required(ErrorMessage = "Ürün ID bilgisi alınamadı")]
        public int ID { get; set; }
    }
}
