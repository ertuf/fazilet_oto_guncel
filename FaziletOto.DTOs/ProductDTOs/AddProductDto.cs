﻿using FaziletOto.DOMAIN.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.ProductDTOs
{
    public class AddProductDto
    {
        [Required(ErrorMessage = "Ürün ismi girilmesi zorunludur")]
        [MaxLength(200,ErrorMessage ="Ürün ismi en fazla 200 karakter içerebilir")]
        [MinLength(2,ErrorMessage ="Ürün açıklamsı en az 2 karakter içerebilir")]
        [DisplayName("Ürün Adı *")]
        public string Name { get; set; }

        [MaxLength(500, ErrorMessage = "Ürün ismi en fazla 500 karakter içerebilir")]
        [DisplayName("Ürün Açıklaması")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Stok miktarı girilmesi zorunludur")]
        [DisplayName("Stok Miktarı *")]
        public string StockAmountAsString { get; set; }
        public decimal StockAmount { get; set; }

        [Required(ErrorMessage = "Standart stok miktarı girilmesi zorunludur")]
        [DisplayName("Standart Stok Miktarı (Olması Gereken) *")]
        public string StandartStockAmountAsString { get; set; }
        public decimal StandartStockAmount { get; set; }

        [Required(ErrorMessage = "Stok ölçü birimi girilmesi zorunludur")]
        [DisplayName("Ürün Ölçü Birimi *")]
        public MeasureType MeasureType { get; set; }

        [Required(ErrorMessage = "Maksimum ürün sipariş miktarı girilmesi zorunludur")]
        [DisplayName("Maksimum ürün sipariş miktarı *")]
        public string MaxOrderAmountAsString { get; set; }
        public decimal MaxOrderAmount { get; set; }

        [Required(ErrorMessage = "Ürün fiyatının girilmesi zorunludur")]
        [DisplayName("Ürün Fiyatı (₺) *")]
        public string PriceAsString { get; set; }
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Ürün birim miktarı girilmelidir")]
        [DisplayName("Ürün Fiyatına Karşılık Gelen Ürün Miktarı *")]
        public string PriceDetermineAsString { get; set; }
        public decimal PriceDetermine { get; set; }

        [Required(ErrorMessage = "İndirim miktarı girilmelidir (İndirim yok ise '0' giriniz)")]
        [DisplayName("İndirim (%)*")]
        public string DiscountAsString { get; set; }
        public decimal Discount { get; set; }

        [Required(ErrorMessage ="En az bir kategori seçimi zorunludur")]
        [DisplayName("Kategoriler")]
        public List<int> CategoryIDs { get; set; }
    }
}
