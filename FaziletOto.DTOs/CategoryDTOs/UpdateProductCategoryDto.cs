﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.CategoryDTOs
{
    public class UpdateProductCategoryDto : AddProductCategoryDto
    {
        [Required(ErrorMessage = "Kategori ID bilgisi alınamadı")]
        public int ID { get; set; }
    }
}
