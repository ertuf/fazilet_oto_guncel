﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.CategoryDTOs
{
    public class AddProductCategoryDto
    {
        [Required(ErrorMessage ="Ürün kategori adı gereklidir")]
        [MaxLength(100, ErrorMessage = "Ürün kategori adı en fazla 100 karakter içerebilir")]
        [DisplayName("Ürün Kategori Adı")]
        public string Name { get; set; }

        [MaxLength(500, ErrorMessage = "Açıklama en fazla 500 karakter içerebilir")]
        [DisplayName("Ürün Kategori Açıklaması")]     
        public string Description { get; set; }
    }
}
