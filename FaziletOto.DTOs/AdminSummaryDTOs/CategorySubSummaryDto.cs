﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.AdminSummaryDTOs
{
    public class CategorySubSummaryDto
    {
        public string CategoryName { get; set; }

        public int RelatedProductCount { get; set; }
    }
}
