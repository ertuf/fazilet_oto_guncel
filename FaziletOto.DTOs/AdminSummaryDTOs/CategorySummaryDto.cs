﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.AdminSummaryDTOs
{
    public class CategorySummaryDto
    {
        public CategorySummaryDto()
        {
            CategorySubSummaryDtos = new List<CategorySubSummaryDto>();
        }

        public List<CategorySubSummaryDto> CategorySubSummaryDtos { get; set; }
    }
}
