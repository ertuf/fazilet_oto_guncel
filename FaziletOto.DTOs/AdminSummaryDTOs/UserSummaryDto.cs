﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.AdminSummaryDTOs
{
    public class UserSummaryDto
    {
        public int AdminCount { get; set; }

        public int SalesmanCount { get; set; }

        public int StockKeeperCount { get; set; }
    }
}
