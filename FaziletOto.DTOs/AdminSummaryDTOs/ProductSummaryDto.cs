﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.AdminSummaryDTOs
{
    public class ProductSummaryDto
    {
        public int ProductCount { get; set; }

        public int Under_50_PercentCount { get; set; }

        public int Under_25_PercentCount { get; set; }

        public int Under_10_PercentCount { get; set; }

        public int Under_5_PercentCount { get; set; }
    }
}
