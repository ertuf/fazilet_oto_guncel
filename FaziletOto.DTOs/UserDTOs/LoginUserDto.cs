﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.UserDTOs
{
    public class LoginUserDto
    {
        [DataType(DataType.EmailAddress)]
        [MaxLength(100,ErrorMessage ="E-posta adresi en fazla 100 karakter içerebilir")]
        [MinLength(8, ErrorMessage = "E-posta adresi en az 8 karakter içerebilir")]
        [Required(ErrorMessage = "E-posta adresini girmek zorunludur")]
        public string Email { get; set; }
       
        [DataType(DataType.Password)]
        [MaxLength(20, ErrorMessage = "Şifre en fazla 20 karakter içerebilir")]
        [MinLength(8, ErrorMessage = "Şifre en az 8 karakter içerebilir")]
        [Required(ErrorMessage = "Şifre girmek zorunludur")]
        public string Password { get; set; }
    }
}
