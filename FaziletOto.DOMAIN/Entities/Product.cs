﻿using FaziletOto.CORE.Model;
using FaziletOto.DOMAIN.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public MeasureType MeasureType { get; set; }
        public decimal StockAmount { get; set; }
        public decimal StandartStockAmount { get; set; }       
        public decimal MaxOrderAmount { get; set; }
        public decimal Price { get; set; }
        public decimal PriceDetermine { get; set; }
        public decimal Discount { get; set; }

        //one to many parent
        public List<BasketItem> BasketItems { get; set; }

        //one to many child
        public int AddingUserID { get; set; }
        public User AddingUser { get; set; }

        //many to many
        public List<ProductAndCategory> ProductAndCategories { get; set; }
        //one to one
    }
}
