﻿using FaziletOto.CORE.Model;
using FaziletOto.DOMAIN.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Entities
{
    public class BasketItem : BaseEntity
    {
        public int Quantity { get; set; }
        public string CustomerNote { get; set; }
        public Situation Situation { get; set; }
        public decimal TotalPrice { get; set; }

        //one to many parent

        //one to many child
        public int ProductID { get; set; }
        public Product Product { get; set; }
        public int? EvaluatingUserID { get; set; }
        public User EvaluatingUser { get; set; }
        public int AddingCustomerID { get; set; }
        public Customer AddingCustomer { get; set; }
        public int BasketID { get; set; }
        public Basket Basket { get; set; }

        //many to many
        //one to one
    }
}
