﻿using FaziletOto.CORE.Model;
using FaziletOto.DOMAIN.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Entities
{
    public class Basket : BaseEntity
    {
        public Decimal TotalPrice { get; set; }
        public Situation Situation { get; set; }

        //one to many parent
        public List<BasketItem> BasketItems { get; set; }

        //one to many child
        public int? EvaluatingUserID { get; set; }
        public User EvaluatingUser { get; set; }
        public int AddedCustomerID { get; set; }
        public Customer AddedCustomer { get; set; }

        //many to many
        //one to one
    }
}
