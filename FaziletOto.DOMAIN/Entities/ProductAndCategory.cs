﻿using FaziletOto.CORE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Entities
{
    public class ProductAndCategory : BaseEntity
    {
        // many to many builder

        public int ProductID { get; set; }
        public Product Product { get; set; }

        public int ProductCategoryID { get; set; }
        public ProductCategory ProductCategory { get; set; }
    }
}
