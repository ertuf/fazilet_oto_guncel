﻿using FaziletOto.CORE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Entities
{
    public class ProductCategory : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        //one to many parent
        //one to many child
        public int AddingUserID { get; set; }
        public User AddingUser { get; set; }

        //many to many
        public List<ProductAndCategory> ProductAndCategories { get; set; }

        //one to one
    }
}
