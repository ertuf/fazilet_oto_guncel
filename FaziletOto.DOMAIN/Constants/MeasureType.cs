﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Constants
{
    public enum MeasureType
    {
        mL, L, gr, kg, adet 
    }
}
