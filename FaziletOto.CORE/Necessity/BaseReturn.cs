﻿using FaziletOto.CORE.Contants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.CORE.Necessity
{
    public class BaseReturn
    {
        public ReturnTitle ReturnTitle { get; set; }

        public string ReturnExplain { get; set; }

        public BaseReturn(ReturnTitle returnTitle, string returnExplain)
        {
            ReturnTitle = returnTitle;
            ReturnExplain = returnExplain;
        }
    }

    public class BaseReturn<TData> : BaseReturn
    {
        public TData Data { get; set; }

        public BaseReturn(TData data, ReturnTitle returnTitle, string returnExplain) : base(returnTitle, returnExplain)
        {
            Data = data;
        }
    }


}
