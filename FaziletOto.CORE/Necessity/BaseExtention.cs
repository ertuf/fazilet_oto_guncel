﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.CORE.Necessity
{
    public static class BaseExtention
    {
        public static string GetInnestException(this Exception ex)
        {
            if (ex.InnerException == null)
            {
                return ex.Message;
            }
            else
            {
                return ex.InnerException.GetInnestException();
            }
        }
    }
}
