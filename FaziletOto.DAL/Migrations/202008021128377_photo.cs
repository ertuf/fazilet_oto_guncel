﻿namespace FaziletOto.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class photo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "PhotoPath", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "PhotoPath");
        }
    }
}
