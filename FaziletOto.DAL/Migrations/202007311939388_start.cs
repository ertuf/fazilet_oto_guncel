﻿namespace FaziletOto.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BasketItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        CustomerNote = c.String(maxLength: 200),
                        Situation = c.Int(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, storeType: "money"),
                        ProductID = c.Int(nullable: false),
                        EvaluatingUserID = c.Int(),
                        AddingCustomerID = c.Int(nullable: false),
                        BasketID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Baskets", t => t.BasketID)
                .ForeignKey("dbo.Customers", t => t.AddingCustomerID)
                .ForeignKey("dbo.Users", t => t.EvaluatingUserID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.EvaluatingUserID)
                .Index(t => t.AddingCustomerID)
                .Index(t => t.BasketID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ContactPersonnel = c.String(nullable: false, maxLength: 50),
                        ContactPhone1 = c.String(maxLength: 50),
                        ContactPhone2 = c.String(maxLength: 50),
                        ContactAddress = c.String(maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 20),
                        Username = c.String(nullable: false, maxLength: 15),
                        ApiKey = c.Guid(nullable: false),
                        LoginKey = c.Guid(),
                        ContactCity = c.String(maxLength: 50),
                        ContactCountry = c.String(maxLength: 50),
                        CompanyName = c.String(nullable: false, maxLength: 200),
                        CompanyPhone1 = c.String(maxLength: 50),
                        CompanyPhone2 = c.String(maxLength: 50),
                        CompanyAddress = c.String(nullable: false, maxLength: 200),
                        CompanyCity = c.String(nullable: false, maxLength: 50),
                        CompanyCountry = c.String(nullable: false, maxLength: 50),
                        CompanyDiscount = c.Double(nullable: false),
                        CanBuy = c.Boolean(nullable: false),
                        CanText = c.Boolean(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        EvaluatingUserID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.EvaluatingUserID)
                .Index(t => t.EvaluatingUserID);
            
            CreateTable(
                "dbo.Baskets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TotalPrice = c.Decimal(nullable: false, storeType: "money"),
                        Situation = c.Int(nullable: false),
                        EvaluatingUserID = c.Int(),
                        AddedCustomerID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.AddedCustomerID)
                .ForeignKey("dbo.Users", t => t.EvaluatingUserID)
                .Index(t => t.EvaluatingUserID)
                .Index(t => t.AddedCustomerID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 75),
                        Surname = c.String(nullable: false, maxLength: 75),
                        Username = c.String(nullable: false, maxLength: 15),
                        Email = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 20),
                        ApiKey = c.Guid(nullable: false),
                        LoginKey = c.Guid(),
                        IsAdmin = c.Boolean(nullable: false),
                        IsSalesman = c.Boolean(nullable: false),
                        IsStockKeeper = c.Boolean(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 500),
                        AddingUserID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.AddingUserID)
                .Index(t => t.AddingUserID);
            
            CreateTable(
                "dbo.ProductAndCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductID = c.Int(nullable: false),
                        ProductCategoryID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.ProductCategories", t => t.ProductCategoryID)
                .Index(t => t.ProductID)
                .Index(t => t.ProductCategoryID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Description = c.String(maxLength: 500),
                        MeasureType = c.Int(nullable: false),
                        StockAmount = c.Decimal(nullable: false, precision: 10, scale: 2),
                        StandartStockAmount = c.Decimal(nullable: false, precision: 10, scale: 2),
                        MaxOrderAmount = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Price = c.Decimal(nullable: false, precision: 10, scale: 2),
                        PriceDetermine = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 10, scale: 2),
                        AddingUserID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.AddingUserID)
                .Index(t => t.AddingUserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BasketItems", "ProductID", "dbo.Products");
            DropForeignKey("dbo.BasketItems", "EvaluatingUserID", "dbo.Users");
            DropForeignKey("dbo.BasketItems", "AddingCustomerID", "dbo.Customers");
            DropForeignKey("dbo.Customers", "EvaluatingUserID", "dbo.Users");
            DropForeignKey("dbo.Baskets", "EvaluatingUserID", "dbo.Users");
            DropForeignKey("dbo.ProductAndCategories", "ProductCategoryID", "dbo.ProductCategories");
            DropForeignKey("dbo.ProductAndCategories", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Products", "AddingUserID", "dbo.Users");
            DropForeignKey("dbo.ProductCategories", "AddingUserID", "dbo.Users");
            DropForeignKey("dbo.BasketItems", "BasketID", "dbo.Baskets");
            DropForeignKey("dbo.Baskets", "AddedCustomerID", "dbo.Customers");
            DropIndex("dbo.Products", new[] { "AddingUserID" });
            DropIndex("dbo.ProductAndCategories", new[] { "ProductCategoryID" });
            DropIndex("dbo.ProductAndCategories", new[] { "ProductID" });
            DropIndex("dbo.ProductCategories", new[] { "AddingUserID" });
            DropIndex("dbo.Baskets", new[] { "AddedCustomerID" });
            DropIndex("dbo.Baskets", new[] { "EvaluatingUserID" });
            DropIndex("dbo.Customers", new[] { "EvaluatingUserID" });
            DropIndex("dbo.BasketItems", new[] { "BasketID" });
            DropIndex("dbo.BasketItems", new[] { "AddingCustomerID" });
            DropIndex("dbo.BasketItems", new[] { "EvaluatingUserID" });
            DropIndex("dbo.BasketItems", new[] { "ProductID" });
            DropTable("dbo.Products");
            DropTable("dbo.ProductAndCategories");
            DropTable("dbo.ProductCategories");
            DropTable("dbo.Users");
            DropTable("dbo.Baskets");
            DropTable("dbo.Customers");
            DropTable("dbo.BasketItems");
        }
    }
}
