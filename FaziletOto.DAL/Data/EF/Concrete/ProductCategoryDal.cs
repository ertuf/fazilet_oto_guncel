﻿using FaziletOto.CORE.Data.EF.Concrete;
using FaziletOto.DAL.Context;
using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Data.EF.Concrete
{
    public class ProductCategoryDal : Repository<ProductCategory,FaziletOtoDbContext>,IProductCategoryDal
    {
        public ProductCategoryDal(FaziletOtoDbContext ctx) : base(ctx)
        {

        }
    }
}
