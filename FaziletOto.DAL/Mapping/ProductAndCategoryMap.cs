﻿using FaziletOto.CORE.Mapping;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Mapping
{
    public class ProductAndCategoryMap : BaseMap<ProductAndCategory>
    {
        public ProductAndCategoryMap()
        {
            ToTable("ProductAndCategories");
            HasRequired(x => x.Product).WithMany(x => x.ProductAndCategories).HasForeignKey(x => x.ProductID).WillCascadeOnDelete(false);
            HasRequired(x => x.ProductCategory).WithMany(x => x.ProductAndCategories).HasForeignKey(x => x.ProductCategoryID).WillCascadeOnDelete(false);
        }
    }
}
