﻿using FaziletOto.CORE.Mapping;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Mapping
{
    public class ProductMap : BaseMap<Product>
    {
        public ProductMap()
        {
            ToTable("Products");
            Property(x => x.Name).IsRequired().HasMaxLength(200);
            Property(x => x.Description).IsOptional().HasMaxLength(500);
            Property(x => x.StockAmount).IsRequired().HasPrecision(10, 2);
            Property(x => x.StandartStockAmount).IsRequired().HasPrecision(10, 2);
            Property(x => x.MeasureType).IsRequired();
            Property(x => x.MaxOrderAmount).IsRequired().HasPrecision(10, 2);
            Property(x => x.Price).IsRequired().IsRequired().HasPrecision(10,2);
            Property(x => x.PriceDetermine).IsRequired().HasPrecision(10, 2);
            Property(x => x.Discount).IsRequired().HasPrecision(10, 2);
          

            //relations
            HasRequired(x => x.AddingUser).WithMany(x => x.AddedProducts).HasForeignKey(x => x.AddingUserID).WillCascadeOnDelete(false);
            //HasMany(x => x.ProductCategories).WithMany(x => x.Products).Map(x => x.MapLeftKey("ProductID").MapRightKey("ProductCategoryID"));

            // x.basketItem in basketItems
        }
    }
}
