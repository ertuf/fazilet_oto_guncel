﻿using FaziletOto.CORE.Mapping;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Mapping
{
    public class CustomerMap : BaseMap<Customer>
    {
        public CustomerMap()
        {
            ToTable("Customers");
            Property(x => x.ContactPersonnel).IsRequired().HasMaxLength(50);
            Property(x => x.ContactPhone1).IsOptional().HasMaxLength(50);
            Property(x => x.ContactPhone2).IsOptional().HasMaxLength(50);
            Property(x => x.ContactAddress).IsOptional().HasMaxLength(200);
            Property(x => x.Email).IsRequired().HasMaxLength(100);
            Property(x => x.Password).IsRequired().HasMaxLength(20);
            Property(x => x.Username).IsRequired().HasMaxLength(15);
            Property(x => x.ApiKey).IsRequired().HasColumnType("uniqueidentifier");
            Property(x => x.LoginKey).IsOptional().HasColumnType("uniqueidentifier");
            Property(x => x.ContactAddress).IsOptional().HasMaxLength(200);
            Property(x => x.ContactCity).IsOptional().HasMaxLength(50);
            Property(x => x.ContactCountry).IsOptional().HasMaxLength(50);
            Property(x => x.CompanyName).IsRequired().HasMaxLength(200);
            Property(x => x.CompanyPhone1).IsOptional().HasMaxLength(50);
            Property(x => x.CompanyPhone2).IsOptional().HasMaxLength(50);
            Property(x => x.CompanyAddress).IsRequired().HasMaxLength(200);
            Property(x => x.CompanyCity).IsRequired().HasMaxLength(50);
            Property(x => x.CompanyCountry).IsRequired().HasMaxLength(50);
            Property(x => x.CompanyDiscount).IsRequired();
            Property(x => x.IsApproved).IsRequired();
            Property(x => x.CanBuy).IsRequired();
            Property(x => x.CanText).IsRequired();

            // relations
            HasRequired(x => x.EvaluatingUser).WithMany(x => x.EvaluatedCustomers).HasForeignKey(x => x.EvaluatingUserID).WillCascadeOnDelete(false);

            // x.AddedBasketItems im BasketItemsMap
            // x.AddedBaskets im BasketsMap
        }
    }
}
