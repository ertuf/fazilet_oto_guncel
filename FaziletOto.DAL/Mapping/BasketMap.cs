﻿using FaziletOto.CORE.Mapping;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Mapping
{
    public class BasketMap : BaseMap<Basket>
    {
        public BasketMap()
        {
            ToTable("Baskets");
            Property(x => x.TotalPrice).IsRequired().HasColumnType("money");
            Property(x => x.Situation).IsRequired();

            // relations
            HasMany(x => x.BasketItems).WithRequired(x => x.Basket).HasForeignKey(x => x.BasketID).WillCascadeOnDelete(false);
            HasOptional(x => x.EvaluatingUser).WithMany(x => x.EvaluatedBaskets).HasForeignKey(x => x.EvaluatingUserID).WillCascadeOnDelete(false);
            HasRequired(x => x.AddedCustomer).WithMany(x => x.AddedBaskets).HasForeignKey(x => x.AddedCustomerID).WillCascadeOnDelete(false);
        }
    }
}
