﻿using FaziletOto.CORE.Mapping;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Mapping
{
    public class ProductCategoryMap : BaseMap<ProductCategory>
    {
        public ProductCategoryMap()
        {
            ToTable("ProductCategories");
            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.Description).IsOptional().HasMaxLength(500);

            //relations
            HasRequired(x => x.AddingUser).WithMany(x => x.AddedProductCategories).HasForeignKey(x => x.AddingUserID).WillCascadeOnDelete(false);
        }
    }
}
