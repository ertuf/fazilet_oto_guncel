﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.CORE.Necessity;
using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DAL.Data.EF.Concrete;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.AdminSummaryDTOs;
using FaziletOto.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Manager
{
    public class UserManager : GenericManager<User, IUserDal>, IUserService
    {
        public UserManager(IUserDal userDal) : base (userDal)
        {

        }

        public BaseReturn<User> AddUserAsAdmin(User user)
        {
            user.IsApproved = true;
            user.ApiKey = Guid.NewGuid();
            return EntityDal.Insert(user);
        }

        public Dictionary<string,object> UpdateUserAsAdmin(User updateUser)
        {          
            Dictionary<string, object> json = new Dictionary<string, object>();

            // e-mail unique test
            if (EntityDal.Gets().Data.Where(x => x.ID != updateUser.ID).Select(x => x.Email).Contains(updateUser.Email))
            {
                json.Add("Result", false);
                json.Add("ID", updateUser.ID);
                json.Add("Explain", "Bu E-posta adresi başkası tarafından kullanılmaktadır");
                return json;
            }


            var baseReturn = EntityDal.Get(x => x.ID == updateUser.ID);
            if (baseReturn.ReturnTitle == ReturnTitle.Success)
            {
                User user = baseReturn.Data;
                user.Name = updateUser.Name;
                user.Surname = updateUser.Surname;
                user.Username = updateUser.Username;
                user.Email = updateUser.Email;
                user.Password = updateUser.Password;
                user.IsActive = updateUser.IsActive;
                user.IsAdmin = updateUser.IsAdmin;
                user.IsSalesman = updateUser.IsSalesman;
                user.IsStockKeeper = updateUser.IsStockKeeper;
                var updateReturn = EntityDal.Update(user);
                if (updateReturn.ReturnTitle == ReturnTitle.Success)
                {
                    json.Add("Result", true);
                    json.Add("ID", user.ID);
                }
                else
                {
                    user = EntityDal.Get(x => x.ID == updateUser.ID).Data;
                    json.Add("Result", false);
                }
            }
            else
            {
                json.Add("Result", false);
            }
            return json;
        }

        public BaseReturn<User> Login(string email, string password)
        {
            var baseReturn = EntityDal.Get(x => x.Email == email && x.Password == password);
            return baseReturn;
        }

        public UserSummaryDto UserSummary()
        {
            int adminCount = 0;
            int salesmanCount = 0;
            int stockKeeperCount = 0;
            List<User> activeUsers = EntityDal.Gets(x=> x.IsActive && !x.IsDeleted).Data;
            foreach (var user in activeUsers)
            {
                if (user.IsAdmin) adminCount++;
                if (user.IsSalesman) salesmanCount++;
                if (user.IsStockKeeper) stockKeeperCount++;
            }
            return new UserSummaryDto()
            {
                AdminCount = adminCount,
                SalesmanCount = salesmanCount,
                StockKeeperCount = stockKeeperCount,
            };
        }
    }
}
