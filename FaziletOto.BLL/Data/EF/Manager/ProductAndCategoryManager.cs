﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.DAL.Data.EF.Concrete;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Manager
{
    public class ProductAndCategoryManager : GenericManager<ProductAndCategory,ProductAndCategoryDal> , IProductAndCategoryService
    {
        public ProductAndCategoryManager(ProductAndCategoryDal productAndCategoryDal):base(productAndCategoryDal)
        {

        }
    }
}
