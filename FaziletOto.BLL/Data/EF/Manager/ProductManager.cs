﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.CORE.Necessity;
using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.AdminSummaryDTOs;
using FaziletOto.DTOs.ProductDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Manager
{
    public class ProductManager : GenericManager<Product, IProductDal>, IProductService
    {
        public ProductManager(IProductDal productDal) : base(productDal)
        {

        }

        public ProductSummaryDto ProductSummary()
        {
            List<Product> ActiveProducts = EntityDal.Gets(x=> x.IsActive && !x.IsDeleted).Data;
            int under50Count = 0;
            int under25Count = 0;
            int under10Count = 0;
            int under5Count = 0;
            foreach (Product product in ActiveProducts)
            {
                if ((float)product.StockAmount / (float)product.StandartStockAmount < 0.05f) under5Count++;
                else if ((float)product.StockAmount / (float)product.StandartStockAmount < 0.1f) under10Count++;
                else if ((float)product.StockAmount / (float)product.StandartStockAmount < 0.25f) under25Count++;
                else if ((float)product.StockAmount / (float)product.StandartStockAmount < 0.50f) under50Count++;
            }
            return new ProductSummaryDto()
            {
                ProductCount = ActiveProducts.Count,
                Under_50_PercentCount = under50Count,
                Under_25_PercentCount = under25Count,
                Under_10_PercentCount = under10Count,
                Under_5_PercentCount = under5Count
            };
        }
    }
}
