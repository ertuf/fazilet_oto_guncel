﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.CategoryDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Manager
{
    public class ProductCategoryManager : GenericManager<ProductCategory, IProductCategoryDal>, IProductCategoryService
    {
        public ProductCategoryManager(IProductCategoryDal productCategoryDal) : base(productCategoryDal)
        {

        }

        public bool UpdateCategoryAsAdmin(ProductCategory updateCategory)
        {
            try
            {
                var baseReturn = EntityDal.Get(x => x.ID == updateCategory.ID);
                if (baseReturn.ReturnTitle == ReturnTitle.Success)
                {
                    ProductCategory category = baseReturn.Data;
                    category.Name = updateCategory.Name;
                    category.Description = updateCategory.Description;
                    var updateReturn = EntityDal.Update(category);
                    if (updateReturn.ReturnTitle == ReturnTitle.Success)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
