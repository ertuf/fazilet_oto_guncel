﻿using FaziletOto.CORE.Necessity;
using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.AdminSummaryDTOs;
using FaziletOto.DTOs.ProductDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Service
{
    public interface IProductService : IProductDal
    {
        ProductSummaryDto ProductSummary();
    }
}
