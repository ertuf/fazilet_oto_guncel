﻿using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.CategoryDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Service
{
    public interface IProductCategoryService : IProductCategoryDal
    {
        bool UpdateCategoryAsAdmin(ProductCategory updateCategory);
    }
}
