﻿using FaziletOto.CORE.Data.EF.Abstract;
using FaziletOto.CORE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Service
{
    public interface IGenericService<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity
    {
    }
}
