﻿using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Service
{
    public interface IProductAndCategoryService : IGenericService<ProductAndCategory>
    {
    }
}
