﻿using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.ProductDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaziletOto.MVC.UI.Models.ModelViews
{
    public class AddProductMV
    {
        public List<ProductCategory> ProductCategories { get; set; }

        public AddProductDto AddProductDto { get; set; }
    }
}