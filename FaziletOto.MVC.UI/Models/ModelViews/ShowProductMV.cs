﻿using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaziletOto.MVC.UI.Models.ModelViews
{
    public class ShowProductMV
    {
        public List<Product> Products { get; set; }
        public List<ProductCategory> Categories { get; set; }
}
}