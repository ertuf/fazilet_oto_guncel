﻿using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.ProductDTOs;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace FaziletOto.MVC.UI.Models.ModelViews
{
    public class ArrangeProductMV
    {
        public List<Product> Products { get; set; }

        public UpdateProductDto UpdateProductDto { get; set; }

        public List<ProductCategory> ProductCategories { get; set; }
    }
}