﻿using FaziletOto.DTOs.AdminSummaryDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaziletOto.MVC.UI.Models.ModelViews
{
    public class AdminSummaryMV
    {
        public UserSummaryDto UserSummaryDto { get; set; }

        public ProductSummaryDto ProductSummaryDto { get; set; }

        public CategorySummaryDto CategorySummaryDto { get; set; }

        //public CustomerSummaryDto CustomerSummaryDto { get; set; }

        //public OrderSummaryDto OrderSummaryDto { get; set; }
    }
}