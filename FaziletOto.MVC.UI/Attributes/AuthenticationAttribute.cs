﻿using FaziletOto.DOMAIN.Entities;
using FaziletOto.MVC.UI.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace FaziletOto.MVC.UI.Attributes
{
    public class AuthenticationAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (UserSession.CurrentUser == null)
            {
                RouteValueDictionary route = new RouteValueDictionary();
                route.Add("controller", "Home");
                route.Add("action", "HomePage");
                filterContext.Result = new RedirectToRouteResult(route);
            }
        }
    }
}