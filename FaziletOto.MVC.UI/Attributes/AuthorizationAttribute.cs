﻿using FaziletOto.DOMAIN.Entities;
using FaziletOto.MVC.UI.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FaziletOto.MVC.UI.Attributes
{
    public class AuthorizationAttribute : FilterAttribute, IActionFilter
    {
        public string Roles { get; set; }
        public string RejectController { get; set; }
        public string RejectAction { get; set; }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isAuthorized = false;
            if (UserSession.CurrentUser != null)
            {
                User user = UserSession.CurrentUser;
                if (user.IsAdmin)
                {
                    if (Roles.Contains("Admin"))
                    {
                        isAuthorized = true;
                    }
                }
                if (user.IsSalesman)
                {
                    if (Roles.Contains("Salesman"))
                    {
                        isAuthorized = true;
                    }
                }
                if (user.IsStockKeeper)
                {
                    if (Roles.Contains("StockKeeper"))
                    {
                        isAuthorized = true;
                    }
                }
                
                if (!isAuthorized)
                {
                    RouteValueDictionary route = new RouteValueDictionary();
                    if (string.IsNullOrEmpty(RejectController) || string.IsNullOrEmpty(RejectAction))
                    {                       
                        route.Add("controller", "Home");
                        route.Add("action", "HomePage");
                        filterContext.Result = new RedirectToRouteResult(route);
                    }
                    else
                    {
                        route.Add("controller", RejectController);
                        route.Add("action", RejectAction);
                        filterContext.Result = new RedirectToRouteResult(route);
                    }
                }
            }
        }
    }
}