﻿using AutoMapper;
using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.DAL.Data.EF.Concrete;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.CategoryDTOs;
using FaziletOto.MVC.UI.Attributes;
using FaziletOto.MVC.UI.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace FaziletOto.MVC.UI.Controllers
{
    [Authentication]
    public class AdminCategoryController : Controller
    {
        private readonly IProductCategoryService categoryManager;

        public AdminCategoryController(IProductCategoryService _categoryManager)
        {
            categoryManager = _categoryManager;
        }

        [HttpGet]
        public ActionResult AdminCategoryPage()
        {
            var baseReturn = categoryManager.Gets(null,"AddingUser");
            return View(baseReturn.Data);
        }

        [Authorization(Roles = "Admin,StockKeeper", RejectAction = "AdminCategoryPage", RejectController ="AdminCategory")]
        [HttpGet]
        public ActionResult AdminCategoryAddPage()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AdminCategoryAddPage(AddProductCategoryDto addProductCategoryDto)
        {
            if (ModelState.IsValid && UserSession.CurrentUser != null)
            {
                // same named product category check
                if (categoryManager.Gets().Data.Select(x => x.Name).Contains(addProductCategoryDto.Name))
                {
                    ModelState.AddModelError(string.Empty, "Aynı isimde kategori mevcuttur");
                    return View(addProductCategoryDto);
                }

                ProductCategory category = Mapper.Map<ProductCategory>(addProductCategoryDto);
                category.AddingUserID = UserSession.CurrentUser.ID;
                var baseReturn = categoryManager.Insert(category);
                if (baseReturn.ReturnTitle == ReturnTitle.Success)
                {
                    return RedirectToAction("AdminCategoryPage", "AdminCategory");
                }
                return View(addProductCategoryDto);
            }
            else
            {
                if (UserSession.CurrentUser == null)
                {
                    ModelState.AddModelError(string.Empty, "Kullanıcı bulunamadı");
                }
                return View(addProductCategoryDto);
            }
        }

        [Authorization(Roles = "Admin,StockKeeper", RejectAction = "AdminCategoryPage", RejectController = "AdminCategory")]
        [HttpGet]
        public ActionResult AdminCategoryArrangePage()
        {
            var baseReturn = categoryManager.Gets(null, "AddingUser");
            return View(baseReturn.Data);
        }


        [HttpPost]
        public JsonResult AdminCategoryUpdateAjax(UpdateProductCategoryDto dto)
        {
            if (ModelState.IsValid)
            {
                ProductCategory updateCategory = Mapper.Map<ProductCategory>(dto);
                bool result = categoryManager.UpdateCategoryAsAdmin(updateCategory);
                return Json(result);
            }
            return Json(false);
        }

        [HttpPost]
        public JsonResult AdminCategoryDeleteAjax(int ID)
        {
            var baseReturn = categoryManager.Get(x => x.ID == ID);
            if (baseReturn.ReturnTitle == ReturnTitle.Success)
            {
                var deleteReturn = categoryManager.UpdateDeletion(baseReturn.Data);
                if (deleteReturn.ReturnTitle == ReturnTitle.Success)
                {
                    return Json(true);
                }
            }
            return Json(false);
        }
    }
}