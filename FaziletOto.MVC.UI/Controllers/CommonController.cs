﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.MVC.UI.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaziletOto.MVC.UI.Controllers
{
    public class CommonController : Controller
    {

        private readonly IUserService userManager;

        public CommonController(IUserService _userManager)
        {
            userManager = _userManager;
        }

        public PartialViewResult RenderNavbar()
        {
            User user = null;
            if (UserSession.CurrentUser != null)
            {
                user = UserSession.CurrentUser;
            }
            return PartialView("~/Views/Shared/NavbarPartial.cshtml", user);
        }
    }
}