﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.UserDTOs;
using FaziletOto.MVC.UI.Attributes;
using FaziletOto.MVC.UI.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FaziletOto.MVC.UI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService userManager;

        public AccountController(IUserService _usermanager)
        {
            userManager = _usermanager;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginPost(LoginUserDto loginUserDto)
        {
            if (ModelState.IsValid)
            {
                var loginReturn = userManager.Login(loginUserDto.Email, loginUserDto.Password);
                if (loginReturn.ReturnTitle == ReturnTitle.Success)
                {
                    User user = loginReturn.Data;
                    if (user.IsActive && !user.IsDeleted)
                    {
                        Guid instantLoginKey = Guid.NewGuid();
                        user.LoginKey = instantLoginKey;
                        var updateReturn = userManager.Update(user);
                        if (updateReturn.ReturnTitle == ReturnTitle.Success)
                        {
                            var getReturn = userManager.Get(x => x.LoginKey == instantLoginKey);
                            if (getReturn.ReturnTitle == ReturnTitle.Success)
                            {
                                UserSession.CurrentUser = getReturn.Data;
                            }
                        }
                    }
                    else
                    {
                        TempData["LoginResult"] = "Kullanıcı silinmiş veya pasif durumdadır, bizimle iletişime geçiniz";
                        return RedirectToAction("HomePage", "Home");
                    }
                }
                else
                {
                    TempData["LoginResult"] = "Kullanıcı bulunamadı";
                    return RedirectToAction("HomePage", "Home");
                }
            }
            TempData["LoginResult"] = "Login olmak için girdiğininiz E-posta ve şifre satırları uygun değildir </br> " +
                "Şifre en az 8 en fazla 20 karakter içermelidir </br> E-posta kurallara uygun olmalıdır";
            return RedirectToAction("HomePage", "Home");
        }

        [Authentication]
        public ActionResult SignOutPost()
        {
            if (UserSession.CurrentUser != null)
            {
                UserSession.CurrentUser = null;
            }
            return RedirectToAction("HomePage","Home");
        }
    }
}