﻿using AutoMapper;
using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.UserDTOs;
using FaziletOto.MVC.UI.Attributes;
using FaziletOto.MVC.UI.Session;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FaziletOto.MVC.UI.Controllers
{
    [Authentication]
    public class AdminUserController : Controller
    {
        private readonly IUserService userManager;

        public AdminUserController(IUserService _userManager)
        {
            userManager = _userManager;
        }

        [Authorization(Roles = "Admin,Salesman,StockKeeper")]
        [HttpGet]
        public ActionResult AdminUserPage()
        {
            var baseReturn = userManager.Gets();
            return View(baseReturn.Data);
        }

        [Authorization(Roles = "Admin", RejectAction = "AdminUserPage", RejectController = "AdminUser")]
        [HttpGet]
        public ActionResult AdminUserAddPage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminUserAddPage(AddUserDto addUserDto)
        {
            if (ModelState.IsValid)
            {
                if (userManager.Gets().Data.Select(x => x.Email).Contains(addUserDto.Email))
                {
                    ModelState.AddModelError("", "Bu E-posta adresi daha önce kullanılmıştır");
                    return View(addUserDto);
                }
                User user = Mapper.Map<User>(addUserDto);
                var baseReturn = userManager.AddUserAsAdmin(user);
                if (baseReturn.ReturnTitle == ReturnTitle.Success)
                {
                    return RedirectToAction("AdminUserPage", "AdminUser");
                }
                return View(addUserDto);
            }
            return View(addUserDto);
        }

        [Authorization(Roles = "Admin,Salesman,StockKeeper")]
        [HttpGet]
        public ActionResult AdminUserArrangePage()
        {
            var model = userManager.Gets();
            return View(model.Data);
        }

        [HttpPost]
        public JsonResult AdminUserUpdateAjax(UpdateUserDto updateUserDto)
        {
            User updateUser = Mapper.Map<User>(updateUserDto);
            var json = userManager.UpdateUserAsAdmin(updateUser);
            bool result = (bool)json["Result"];
            if (result)
            {
                if (updateUserDto.Photo != null && updateUserDto.Photo.ContentLength <= 5242880)
                {
                    try
                    {
                        string fileExtension = Path.GetFileName(updateUserDto.Photo.FileName).Split('.').Reverse().ToArray()[0];
                        string photoPath = Path.Combine(Server.MapPath("~/Images/Upload/UserProfileImg"), $"UserProfilePhoto_{updateUserDto.ID}.{fileExtension}");
                        updateUserDto.Photo.SaveAs(photoPath);
                        User user = userManager.Get(x => x.ID == updateUser.ID).Data;
                        user.PhotoPath = $"/Images/Upload/UserProfileImg/UserProfilePhoto_{user.ID}.{fileExtension}";
                        userManager.Update(user);
                        json.Add("Photo", true);
                    }
                    catch
                    {
                        json.Add("Photo", false);
                    }
                }
            }
            return Json(json);
        }

        [HttpPost]
        public JsonResult AdminUserDeleteAjax(int ID)
        {
            var baseResult = userManager.UpdateDeletion(userManager.Get(x => x.ID == ID).Data);
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("ID", ID.ToString());

            // if user have deleted yourself
            if (UserSession.CurrentUser.ID == ID)
            {
                UserSession.CurrentUser = null;
            }

            return Json(dictionary);
        }
    }
}