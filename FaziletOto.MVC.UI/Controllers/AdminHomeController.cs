﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.AdminSummaryDTOs;
using FaziletOto.MVC.UI.Attributes;
using FaziletOto.MVC.UI.Models.ModelViews;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaziletOto.MVC.UI.Controllers
{
    [Authentication]  
    public class AdminHomeController : Controller
    {
        readonly private IUserService userManager;
        readonly private IProductCategoryService categoryManager;
        readonly private IProductService productManager;
        readonly private IProductAndCategoryService relationManager;


        public AdminHomeController(IUserService userManager, IProductCategoryService categoryManager, IProductService productManager, IProductAndCategoryService relationManager)
        {
            this.userManager = userManager;
            this.categoryManager = categoryManager;
            this.productManager = productManager;
            this.relationManager = relationManager;
        }

        [Authorization(Roles = "Admin,Salesman,StockKeeper")]
        [HttpGet]
        public ActionResult AdminHomePage()
        {
            CategorySummaryDto categorySummary = new CategorySummaryDto();
            var categories = categoryManager.Gets(x => x.IsActive && !x.IsDeleted, "ProductAndCategories.Product").Data;
            foreach (var category in categories)
            {
                CategorySubSummaryDto categorySubSummary = new CategorySubSummaryDto();
                categorySubSummary.CategoryName = category.Name;
                categorySubSummary.RelatedProductCount = category.ProductAndCategories.Where(x => x.IsActive && !x.IsDeleted).ToList().Count;
                categorySummary.CategorySubSummaryDtos.Add(categorySubSummary);
            }

            AdminSummaryMV adminSummaryMV = new AdminSummaryMV()
            {
                UserSummaryDto = userManager.UserSummary(),
                ProductSummaryDto = productManager.ProductSummary(),
                CategorySummaryDto = categorySummary,
            };

            return View(adminSummaryMV);
        }
    }
}