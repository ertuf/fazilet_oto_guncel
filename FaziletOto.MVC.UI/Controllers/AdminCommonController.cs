﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaziletOto.MVC.UI.Controllers
{
    public class AdminCommonController : Controller
    {
        public PartialViewResult RenderSidebar(string pageTitle)
        {
            return PartialView("~/Views/Shared/AdminSidebarPartial.cshtml",pageTitle);
        }
    }
}