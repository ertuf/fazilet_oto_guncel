﻿using AutoMapper;
using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.ProductDTOs;
using FaziletOto.MVC.UI.Attributes;
using FaziletOto.MVC.UI.Models.ModelViews;
using FaziletOto.MVC.UI.Session;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FaziletOto.MVC.UI.Controllers
{
    [Authentication]
    public class AdminProductController : Controller
    {
        readonly private IProductCategoryService categoryManager;
        readonly private IProductService productManager;
        readonly private IProductAndCategoryService relationManager;

        public AdminProductController(IProductCategoryService _categoryManager, IProductService _productManager, IProductAndCategoryService _relationManager)
        {
            categoryManager = _categoryManager;
            productManager = _productManager;
            relationManager = _relationManager;
        }

        [HttpGet]
        public ActionResult AdminProductHomePage()
        {
            ShowProductMV showProductMV = new ShowProductMV()
            {
                Products = productManager.Gets(null, "ProductAndCategories", "AddingUser").Data,
                Categories = categoryManager.Gets(x => x.IsActive && !x.IsDeleted).Data
            };
            return View(showProductMV);
        }

        [Authorization(Roles = "Admin,StockKeeper", RejectAction = "AdminProductHomePage", RejectController = "AdminProduct")]
        [HttpGet]
        public ActionResult AdminProductAddPage(AddProductDto addProductDto = null)
        {
            AddProductMV addProductMV = new AddProductMV();
            var categoryReturn = categoryManager.Gets(x => x.IsActive && !x.IsDeleted);
            var categories = categoryReturn.Data;
            addProductMV.ProductCategories = categories;
            if (addProductDto != null)
            {
                addProductMV.AddProductDto = addProductDto;
            }
            else
            {
                addProductMV.AddProductDto = null;
            }
            return View(addProductMV);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminProductAddPage(AddProductMV addProductMV, params int[] CategoryID)
        {
            AddProductDto dto = addProductMV.AddProductDto;
            try
            {
                dto.StockAmount = Convert.ToDecimal(dto.StockAmountAsString.Replace('.', ','));
                dto.StandartStockAmount = Convert.ToDecimal(dto.StandartStockAmountAsString.Replace('.', ','));
                dto.MaxOrderAmount = Convert.ToDecimal(dto.MaxOrderAmountAsString.Replace('.', ','));
                dto.Price = Convert.ToDecimal(dto.PriceAsString.Replace('.', ','));
                dto.PriceDetermine = Convert.ToDecimal(dto.PriceDetermineAsString.Replace('.', ','));
                dto.Discount = Convert.ToDecimal(dto.DiscountAsString.Replace('.', ','));
                dto.CategoryIDs = new List<int>();
            }
            catch (Exception)
            {
                return RedirectToAction("AdminProductAddPage", "AdminProduct", addProductMV.AddProductDto);
            }
            Product product = Mapper.Map<Product>(dto);
            product.AddingUserID = UserSession.CurrentUser.ID;
            var baseReturn = productManager.Insert(product);
            if (baseReturn.ReturnTitle == ReturnTitle.Success)
            {
                List<ProductAndCategory> currentRelations = new List<ProductAndCategory>();
                foreach (int categoryID in CategoryID)
                {
                    ProductAndCategory currentRelation = new ProductAndCategory()
                    {
                        ProductID = product.ID,
                        ProductCategoryID = categoryID,
                    };
                    var relationResult = relationManager.Insert(currentRelation);
                    if (relationResult.ReturnTitle != ReturnTitle.Success)
                    {
                        productManager.UpdateDeletion(product);
                        foreach (var _currentRelation in currentRelations)
                        {
                            relationManager.UpdateDeletion(_currentRelation);
                        }
                        return RedirectToAction("AdminProductAddPage", "AdminProduct", addProductMV.AddProductDto);
                    }
                    else
                    {
                        currentRelations.Add(relationResult.Data);
                    }
                }
                return RedirectToAction("AdminProductHomePage", "AdminProduct");
            }
            return RedirectToAction("AdminProductAddPage", "AdminProduct", addProductMV.AddProductDto);
        }

        [HttpGet]
        [Authorization(Roles = "Admin,StockKeeper",RejectAction ="AdminProductHomePage", RejectController ="AdminProduct")]
        public ActionResult AdminProductArrangePage()
        {          
            var products = productManager.Gets(x => !x.IsDeleted && x.IsActive, "ProductAndCategories.ProductCategory", "AddingUser").Data;
            var productCategories = categoryManager.Gets(x => !x.IsDeleted && x.IsActive).Data;
            ArrangeProductMV arrangeProductMV = new ArrangeProductMV();
            arrangeProductMV.Products = products;
            arrangeProductMV.ProductCategories = productCategories;
            return View(arrangeProductMV);
        }

        [HttpPost]
        public JsonResult AdminProductDeletaAjax(int ID)
        {
            try
            {
                var baseReturn = productManager.Get(x => x.ID == ID, "ProductAndCategories.ProductCategory");
                if (baseReturn.ReturnTitle == ReturnTitle.Success)
                {
                    var deleteReturn = productManager.UpdateDeletion(baseReturn.Data);
                    if (deleteReturn.ReturnTitle == ReturnTitle.Success)
                    {
                        var productAndCategories = productManager.Get(x => x.ID == ID, "ProductAndCategories.ProductCategory").Data.ProductAndCategories;
                        foreach (var productAndCategory in productAndCategories)
                        {
                            if (productAndCategory.IsActive)
                            {
                                productAndCategory.IsActive = false;
                                categoryManager.Get(x => x.ID == productAndCategory.ProductCategoryID, "ProductAndCategories").Data.ProductAndCategories.First(x => x.ProductID == baseReturn.Data.ID).IsActive = false;
                                relationManager.Update(productAndCategory);                             
                            }
                        }
                        return Json(true);
                    }
                }
                return Json(false);
            }
            catch (Exception)
            {
                return Json(false);
            }          
        }

        [HttpPost]
        public JsonResult AdminProductUpdateAjax(UpdateProductDto updateProductDto, string CategoryIDsAsString)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();

            if (categoryManager.Gets(x => !x.IsDeleted && x.ID != updateProductDto.ID).Data.Select(x => x.Name).Contains(updateProductDto.Name))
            {
                response.Add("result", false);
                response.Add("explain", "Aynı isme sahip ürün mevcuttur");
            }

            Product product = productManager.Get(x => x.ID == updateProductDto.ID, "ProductAndCategories.ProductCategory").Data;
            try
            {
                List<int> categoryIDs = new List<int>();
                List<int> saveCategoryIDs = new List<int>();
                if (!String.IsNullOrEmpty(CategoryIDsAsString))
                {
                    foreach (var id in CategoryIDsAsString.Split(','))
                    {
                        categoryIDs.Add(int.Parse(id));
                        saveCategoryIDs.Add(int.Parse(id));
                    }                   
                } 
                
                product.Name = updateProductDto.Name;
                product.Description = updateProductDto.Description;
                product.MeasureType = updateProductDto.MeasureType;
                product.StockAmount = decimal.Parse(updateProductDto.StockAmountAsString.Replace('.', ','));
                product.StandartStockAmount = decimal.Parse(updateProductDto.StandartStockAmountAsString.Replace('.', ','));
                product.MaxOrderAmount = decimal.Parse(updateProductDto.MaxOrderAmountAsString.Replace('.', ','));
                product.Price = decimal.Parse(updateProductDto.PriceAsString.Replace('.', ','));
                product.PriceDetermine = decimal.Parse(updateProductDto.PriceDetermineAsString.Replace('.', ','));
                product.Discount = decimal.Parse(updateProductDto.DiscountAsString.Replace('.', ','));
                var updateReturn = productManager.Update(product);
                if (updateReturn.ReturnTitle == ReturnTitle.Success)
                {
                    List<ProductAndCategory> relations = product.ProductAndCategories;
                    List<ProductCategory> categories = categoryManager.Gets(x => x.IsActive && !x.IsDeleted, "ProductAndCategories").Data;
                    foreach (var relation in relations)
                    {
                        if (categoryIDs.Contains(relation.ProductCategoryID))
                        {
                            if (!relation.IsActive)
                            {
                                relation.IsActive = true;
                                categories.First(x => x.ID == relation.ProductCategoryID).ProductAndCategories.First(x => x.ProductID == product.ID).IsActive = true;
                                relationManager.Update(relation);
                            }
                            categoryIDs.Remove(relation.ProductCategoryID);
                        }
                        else
                        {
                            if (relation.IsActive)
                            {
                                relation.IsActive = false;
                                categories.First(x => x.ID == relation.ProductCategoryID).ProductAndCategories.First(x => x.ProductID == product.ID).IsActive = false;
                                relationManager.Update(relation);
                            }
                        }
                    }
                    if (categoryIDs.Count > 0)
                    {
                        foreach (var categoryID in categoryIDs)
                        {
                            ProductAndCategory newRelation = new ProductAndCategory()
                            {
                                ProductID = product.ID,
                                ProductCategoryID = categoryID,
                            };
                            relationManager.Insert(newRelation);
                        }
                    }

                    List<string> categoriesNames = new List<string>();

                    categoriesNames = categoryManager.Gets(x => x.IsActive && !x.IsDeleted && saveCategoryIDs.Contains(x.ID)).Data.Select(w=> w.Name).ToList();
                    var jsonSerialiser = new JavaScriptSerializer();
                    var jsonCategoriesNames = jsonSerialiser.Serialize(categoriesNames);

                    response.Add("result", true);
                    response.Add("categoriesNames", jsonCategoriesNames);
                    return Json(response);
                }
                response.Add("result", false);
                response.Add("explain", "<li class=\"list-group-item-danger\">Verileri şekilde tekrar giriniz.<br/>Sayfayı güncelleyiniz.<br/>Hata devam ederse bizimle iletişime geçiniz.</li>");
                product = productManager.Get(x => x.ID == updateProductDto.ID).Data;
                return Json(response);
            }
            catch (Exception)
            {
                response.Add("result", false);
                response.Add("explain", "<li class=\"list-group-item-danger\">Verileri dikkatli şekilde tekrar giriniz.<br/>Sayfayı güncelleyiniz.<br/>Hata devam ederse bizimle iletişime geçiniz.</li>");
                product = productManager.Get(x => x.ID == updateProductDto.ID).Data;
                return Json(response);
            }          
        }
    }
}