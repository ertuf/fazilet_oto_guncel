﻿using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaziletOto.MVC.UI.Session
{
    public static class UserSession
    {
        public static User CurrentUser 
        { 
            get {return HttpContext.Current.Session["CurrentUser"] as User; } 
            set {HttpContext.Current.Session["CurrentUser"] = value; } 
        }
    }
}