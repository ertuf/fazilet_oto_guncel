﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using FaziletOto.INFRASTRUCTURE.AutoMapperProfiles;
using FaziletOto.INFRASTRUCTURE.DependencyModules;
using Ninject;
using Ninject.Web.Common.WebHost;

namespace FaziletOto.MVC.UI
{
    public class MvcApplication : NinjectHttpApplication
    {
        protected override void OnApplicationStarted()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Mapper.Initialize(cfg => cfg.AddProfile(new AutoMapProfile()));
            base.OnApplicationStarted();
        }

        protected override IKernel CreateKernel()
        {
            return new StandardKernel(new DependencyBll(),new DependencyUI());
        }
    }
}
